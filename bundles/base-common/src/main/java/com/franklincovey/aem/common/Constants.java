package com.franklincovey.aem.common;

/**
 * The class contains common application constants.
 */
public final class Constants {

    /**
     * EMPTY String.
     */
    public static final String EMPTY_STRING = "";

    /**
     * EMPTY Space String.
     */
    public static final String EMPTY_SPACE_STRING = " ";

    /**
     * Charset name.
     */
    public static final String CHARSET_NAME = "UTF-8";

    /**
     * The content node name used in AEM.
     */
    public static final String JCR_CONTENT = "jcr:content";

    /**
     * New line character.
     */
    public static final String NEW_LINE = "\n";

    /**
     * HTML extension.
     */
    public static final String HTML_EXTENSION = ".html";

    /**
     * JSON content type.
     */
    public static final String JSON_CONTENT_TYPE = "application/json";

    /**
     * Instantiates a new constants.
     */
    private Constants() {
    }

}
