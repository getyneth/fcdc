package com.franklincovey.aem.sightly.providers;

import com.adobe.cq.sightly.WCMUsePojo;
import com.franklincovey.aem.services.LinkURLTypeCheckerService;

/**
 * @author manish.singh
 *
 */

public class LinkURLTypeProvider extends WCMUsePojo{
	
	private static final String URL_PROPERTY_NAME = "url";
	private static final String COLOR_PROPERTY_NAME = "color";
	private static final String COLOR_BLUE = "rgba(62,109,137,1)";
	private static final String COLOR_BLUE_MEDIUM = "rgba(51,144,172,1)";
	private static final String COLOR_BLUE_LIGHT = "rgba(79,162,210,1)";
	private static final String CSS_CLASS_BLUE = "btn-style btn-color-blue margin";
	private static final String CSS_CLASS_BLUE_MEDIUM = "btn-style btn-color-blue-medium";
	private static final String CSS_CLASS_BLUE_LIGHT = "btn-style btn-color-blue-light";
	private static final String PAGE_SUFFIX = ".html";
	private boolean linkOpensInNewTab = false;
	private String cssClass;
	private String linkURL;

	@Override
	public void activate() throws Exception {
		LinkURLTypeCheckerService linkService = getSlingScriptHelper()
                .getService(LinkURLTypeCheckerService.class);
		linkURL = get(URL_PROPERTY_NAME, String.class);
		linkURL = linkService.resolveLinkURLSuffix(getResourceResolver(), linkURL);
		if(linkURL.contains(PAGE_SUFFIX))
			linkOpensInNewTab = true;
		String selectedColor = get(COLOR_PROPERTY_NAME, String.class);
		setCssClass(selectedColor);
	}

	/**
	 * @return the linkOpensInNewTab
	 */
	public boolean isLinkOpensInNewTab() {
		return linkOpensInNewTab;
	}

	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}

	/**
	 * @param cssClass the cssClass to set
	 */
	public void setCssClass(String selectedColor) {
		if(selectedColor.equalsIgnoreCase(COLOR_BLUE))
			this.cssClass = CSS_CLASS_BLUE;
		else if(selectedColor.equalsIgnoreCase(COLOR_BLUE_MEDIUM))
			this.cssClass = CSS_CLASS_BLUE_MEDIUM;
		else if(selectedColor.equalsIgnoreCase(COLOR_BLUE_LIGHT))
			this.cssClass = CSS_CLASS_BLUE_LIGHT;
		else
			this.cssClass = "";
	}

	/**
	 * @return the linkURL
	 */
	public String getLinkURL() {
		return linkURL;
	}

}
