package com.franklincovey.aem.services.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;

import com.day.cq.wcm.api.Page;
import com.franklincovey.aem.services.LinkURLTypeCheckerService;

/*
 * Class to resolve URL suffix for a Page Resource, if it is a JCR path, .html would be appended as suffix
 */

/**
 * @author manish.singh
 */
@Component
@Service
@Properties({ @Property(name = "service.vendor", value = "com.franklincovey.aem.services"),
        @Property(name = "service.description", value = "com.franklincovey.aem.services Page Suffix Resolver Service")})
public class LinkURLTypeCheckerServiceImpl implements LinkURLTypeCheckerService{
	
	private static final String PAGE_SUFFIX = ".html";

    /**
     * Validate the URL and append .html extension if it is an AEM page.
     *
     * @param resolver
     *            resource resolver to resolve the link
     * @param link
     *            resource link to be resolved
     * @return String
     */

	@Override
	public String resolveLinkURLSuffix(ResourceResolver resolver, String linkURL) {
		if (resolver.getResource(linkURL) != null
                && !ResourceUtil.isNonExistingResource(resolver.getResource(linkURL))
                && resolver.getResource(linkURL).adaptTo(Page.class) != null) {
			return linkURL.concat(PAGE_SUFFIX);
        }
		return linkURL;
	}
}
