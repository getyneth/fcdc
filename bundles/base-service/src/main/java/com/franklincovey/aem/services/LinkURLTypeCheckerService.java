package com.franklincovey.aem.services;

import org.apache.sling.api.resource.ResourceResolver;

public interface LinkURLTypeCheckerService {
	String resolveLinkURLSuffix(final ResourceResolver resolver, final String linkURL);
}
