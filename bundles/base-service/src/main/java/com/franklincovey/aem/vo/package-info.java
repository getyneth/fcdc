/**
 * This packages consists the Value object classes or model classes.
 *
 */
/**
 * @author manish.singh
 *
 */
package com.franklincovey.aem.vo;
